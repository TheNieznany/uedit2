# My second attempt at making a simple text editor for Linux terminal.
# Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
#
# This file is part of uedit2.
#
# uedit2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uedit2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uedit2.  If not, see <https://www.gnu.org/licenses/>.

CXX = g++

CXXFLAGS := -fmax-errors=4 -fno-common -fstack-usage
CXXFLAGS := $(CXXFLAGS) -ggdb -g3 -Og
CXXFLAGS := $(CXXFLAGS) -Wall -Wconversion -Wdouble-promotion -Wextra -Wformat=2
CXXFLAGS := $(CXXFLAGS) -Wformat-overflow -Wformat-truncation -Wshadow
CXXFLAGS := $(CXXFLAGS) -Wstack-usage=8192 -Wundef

SRCDIR = src
INCDIR = include
OBJDIR = build



# DO NOT EDIT BELOW THIS LINE!

sources = $(shell find $(SRCDIR) -name "*.cpp")
objects = $(sources:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)



uedit: $(objects)
	$(CXX) $^ -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(INCDIR)/%.hpp
	$(CXX) $(CXXFLAGS) -I $(INCDIR) -c $< -o $@

clean:
	$(RM) $(objects) $(OBJDIR)/*.su
