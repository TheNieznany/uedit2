/*
 * My second attempt at making a simple text editor for Linux terminal.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit2.
 *
 * uedit2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit2.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "backend.hpp"

Backend::Backend()
{
	this->enableRawMode();
	this->getTerminalSize();
}

Backend::~Backend()
{
	this->disableRawMode();
}



void Backend::quit(const char* msg)
{
	this->disableRawMode();
	this->clearScreen();
	printf("[!] %s\n", msg);
	exit(1);
}



void Backend::enableRawMode()
{
	if (tcgetattr(STDIN_FILENO, &orig_termios) == -1)
	{
		quit("tcgetattr() failed at backend.cpp:27");
	}
	
	termios raw = orig_termios;
	
	raw.c_iflag &= ~(IXON);
	raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
	
	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1)
	{
		quit("tcsetattr() failed at backend.cpp:37");
	}
}

void Backend::disableRawMode()
{
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios);
}



void Backend::getTerminalSize()
{
	winsize ws;
	
	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0)
	{
		quit("ioctl() failed at backend.cpp:54");
	}
	else
	{
		this->cols = ws.ws_col;
		this->rows = ws.ws_row;
	}
}



void Backend::clearScreen()
{
	fputs("\x1b[1;1H\x1b[0J", stdout);
}

void Backend::moveCursor(size_t x, size_t y)
{
	printf("\x1b[%zu;%zuH", y + 1, x + 1);
}

void Backend::setColor(size_t fg, size_t bg)
{
	printf("\x1b[%zu;%zum", fg + 30, bg + 40);
}

void Backend::resetColor()
{
	fputs("\x1b[0m", stdout);
}

void Backend::invertColor()
{
	fputs("\x1b[7m", stdout);
}



int Backend::getKey()
{
	int c = getchar();
	
	if (c == -1)
	{
		return 0;
	}
	else if (c == 27)
	{
		int c2 = getchar();
		int c3 = getchar();
		
		if (c2 == 0 && c3 == 0)
		{
			 return ESCAPE;
		}
		else if (c2 == 91)
		{
			switch (c3)
			{
			case 51: if (getchar() == 126) return DELETE; else break;
			case 53: if (getchar() == 126) return PAGE_UP; else break;
			case 54: if (getchar() == 126) return PAGE_DOWN; else break;
			
			case 65: return ARROW_UP;
			case 66: return ARROW_DOWN;
			case 67: return ARROW_RIGHT;
			case 68: return ARROW_LEFT;
			
			case 72: return HOME;
			case 70: return END;
			}
		}
		
		return 0;
	}
	else
	{
		return c;
	}
}
