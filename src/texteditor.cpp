/*
 * My second attempt at making a simple text editor for Linux terminal.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit2.
 *
 * uedit2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit2.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "texteditor.hpp"

TextEditor::TextEditor()
{
	backend = new Backend{};
	
	running = true;
	tabWidth = 4;

	cursor = {0, 0};
	scroll = {0, 0};
	term = {0, 0};
	
	dirty = false;
}

TextEditor::~TextEditor()
{
	delete backend;
}



void TextEditor::newBuffer()
{
	lines.clear();
	lines.emplace_back("");
	
	filename = "";
}

bool TextEditor::loadFile(const std::string& filepath)
{
	lines.clear();
	
	std::fstream file;
	file.open(filepath, std::ios::in);
	if (not file.is_open()) return false;
	
	std::string s;
	
	for (size_t i = 0; file.good(); i++)
	{
		std::getline(file, s);
		lines.push_back(s);
	}
	
	file.close();
	
	filename = filepath.substr(filepath.find_last_of('/') + 1);
	return true;
}

bool TextEditor::saveFile()
{
	std::fstream file;
	
	if (filename == "")
	{
		backend->clearScreen();
		fputs("Enter filename: ", stdout);
		
		while (true)
		{
			int c = getchar();
			if (c == '\n')
			{
				putchar(c);
				break;
			}
			
			if ((32 <= c && c <= 126) && c != '*' && c != '/')
			{
				putchar(c);
				filename.push_back(static_cast<char>(c));
			}
		}
		
		file.open(filename, std::ios::in);
		if (file.is_open())
		{
			backend->setColor(RED, BLACK);
			fputs("Warning!", stdout);
			backend->resetColor();
			printf(" File %s already exists\n", filename.c_str());
			fputs("Overwrite? [y/N] ", stdout);
			
			int c = getchar();
			
			if (c != 'y' && c != 'Y')
			{
				file.close();
				filename = "";
				return true;
			}
			
			file.close();
		}
	}
	
	
	
	file.open(filename, std::ios::out);
	if (not file.is_open()) return false;
	
	for (size_t i = 0; i < lines.size(); i++)
	{
		file << lines.at(i) << '\n';
	}
	
	file.close();
	dirty = false;
	return true;
}





void TextEditor::loop()
{
	while (running)
	{
		display();
		handleInput();
	}
	
	backend->clearScreen();
}



void TextEditor::display()
{
	backend->clearScreen();
	
	updateScrollCursor();
	updateTerminalCursor();
	displayLinesWithTabs();
	displayStatusBar();
	
	backend->moveCursor(term.x, term.y);
}





void TextEditor::updateScrollCursor()
{
	if (cursor.y < scroll.y)
	{
		scroll.y = cursor.y;
	}
	else if (cursor.y >= scroll.y + (backend->rows - 2))
	{
		scroll.y = cursor.y - backend->rows + 2;
	}
	
	if (cursor.x < scroll.x)
	{
		scroll.x = cursor.x;
	}
	else if (cursor.x >= scroll.x + backend->cols)
	{
		scroll.x = cursor.x - backend->cols + 1;
	}
}

void TextEditor::updateTerminalCursor()
{
	term.x = cursor.x - scroll.x;
	term.y = cursor.y - scroll.y;
	
	size_t spaces = 0;
	size_t spaceIndex = 0;
	
	for (size_t i = 0; i < cursor.x; i++, spaceIndex++)
	{
		if (lines.at(cursor.y).at(i) == TAB)
		{
			// no clue how this works tho lmao (from first version of uedit btw)
			spaces += (tabWidth - 1) - (spaceIndex % tabWidth);
			spaceIndex += (tabWidth - 1) - (spaceIndex % tabWidth);
		}
	}
	
	term.x += spaces;
}

void TextEditor::displayLinesWithTabs()
{
	size_t limitY = (backend->rows - 1) + scroll.y;
	size_t limitX = backend->cols + scroll.x;
	
	for (size_t i = scroll.y; i < lines.size() && i < limitY; i++)
	{
		size_t tmp = 0;
		
		for (size_t j = scroll.x; j < lines.at(i).size() && j < limitX; j++)
		{
			if (lines.at(i).at(j) == TAB)
			{
				do
				{
					putchar(' ');
					tmp++;
				}
				while (tmp % tabWidth != 0);
			}
			else
			{
				putchar(lines.at(i).at(j));
				tmp++;
			}
		}
		
		putchar('\n');
	}
}

void TextEditor::displayStatusBar()
{
	std::string str1;
	
	backend->moveCursor(0, backend->rows - 1);
	backend->invertColor();
	
	if (dirty) str1 = "* ";
	else str1 = "  ";
	
	if (filename == "")
	{
		str1 += "-UNTITLED-";
	}
	else
	{
		str1 += filename;
	}
	
	printf("%-*s", static_cast<int>(backend->cols) / 2, str1.c_str());
	
	std::string str2 = std::to_string(lines.size()) + " lines";
	printf("%*s", static_cast<int>(backend->cols) / 2, str2.c_str());
	
	backend->resetColor();
}





void TextEditor::handleInput()
{
	int key = backend->getKey();
	if (key == 0) return;
	
	switch (key)
	{
	case CTRL_Q:
		if (dirty)
		{
			backend->clearScreen();
			
			backend->setColor(RED, BLACK);
			fputs("Warning!", stdout);
			backend->resetColor();
			printf(" Quit without saving %s? [y/N] ", filename.c_str());
			
			int c = getchar();
			
			if (c != 'n' && c != 'N')
			{
				saveFile();
			}
		}
		
		running = false;
		break;
	
	case CTRL_S:
		saveFile();
		break;
	
	
	
	
	
	case BACKSPACE:
		if (cursor.x != 0)
		{
			lines.at(cursor.y).erase(cursor.x - 1, 1);
			cursor.x--;
		}
		else if (cursor.y != 0)
		{
			lines.at(cursor.y - 1) += lines.at(cursor.y);
			lines.erase(lines.begin() + cursor.y);
			cursor.x = lines.at(cursor.y - 1).size();
			cursor.y--;
		}
		
		dirty = true;
		break;
	
	case ENTER:
		if (cursor.x < lines.at(cursor.y).size())
		{
			lines.insert(lines.begin() + cursor.y + 1, "");
			lines.at(cursor.y + 1) += lines.at(cursor.y).substr(cursor.x);
			lines.at(cursor.y).erase(cursor.x);
			cursor.x = 0;
			cursor.y++;
		}
		else
		{
			lines.insert(lines.begin() + cursor.y + 1, "");
			cursor.x = 0;
			cursor.y++;
		}
		
		dirty = true;
		break;
	
	case DELETE:
		if (cursor.x != lines.at(cursor.y).size())
		{
			lines.at(cursor.y).erase(cursor.x, 1);
		}
		else if (cursor.y != lines.size() - 1)
		{
			lines.at(cursor.y) += lines.at(cursor.y + 1);
			lines.erase(lines.begin() + cursor.y + 1);
		}
		
		dirty = true;
		break;
	
	case HOME:
		cursor.x = 0;
		break;
	
	case END:
		cursor.x = lines.at(cursor.y).size();
		break;
	
	
	
	
	
	case ARROW_UP:
		if (cursor.y != 0)
		{
			cursor.y--;
			
			if (cursor.x > lines.at(cursor.y).size())
			{
				cursor.x = lines.at(cursor.y).size();
			}
		}
		break;
	
	case ARROW_DOWN:
		if (cursor.y != lines.size() - 1)
		{
			cursor.y++;
			
			if (cursor.x > lines.at(cursor.y).size())
			{
				cursor.x = lines.at(cursor.y).size();
			}
		}
		break;
	
	case ARROW_LEFT:
		if (cursor.y != 0)
		{
			if (cursor.x == 0)
			{
				cursor.x = lines.at(cursor.y - 1).size();
				cursor.y--;
			}
			else
			{
				cursor.x--;
			}
		}
		else if (cursor.x != 0)
		{
			cursor.x--;
		}
		break;
	
	case ARROW_RIGHT:
		if (cursor.y != lines.size() - 1)
		{
			if (cursor.x == lines.at(cursor.y).size())
			{
				cursor.x = 0;
				cursor.y++;
			}
			else
			{
				cursor.x++;
			}
		}
		else if (cursor.x != lines.at(cursor.y).size())
		{
			cursor.x++;
		}
		break;
	
	
	
	default:
		if ((32 <= key && key <= 126) || key == TAB)
		{
			lines.at(cursor.y).insert(
				lines.at(cursor.y).begin() + cursor.x,
				static_cast<char>(key)
			);
			cursor.x++;
			
			dirty = true;
		}
	}
}
