/*
 * My second attempt at making a simple text editor for Linux terminal.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit2.
 *
 * uedit2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit2.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdio>
#include <cstdlib>

#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#include "keys.hpp"





class Backend
{
public:
	termios orig_termios;
	
	size_t cols;
	size_t rows;



	Backend();
	~Backend();



	void quit(const char* msg);

	void enableRawMode();
	void disableRawMode();
	
	void getTerminalSize();
	
	void clearScreen();
	void moveCursor(size_t x, size_t y);
	void setColor(size_t fg, size_t bg);
	void resetColor();
	void invertColor();
	
	int getKey();
};
