/*
 * My second attempt at making a simple text editor for Linux terminal.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit2.
 *
 * uedit2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit2.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <fstream>
#include <string>
#include <vector>

#include "backend.hpp"
#include "keys.hpp"
#include "colors.hpp"



struct Cursor
{
	size_t x;
	size_t y;
};



class TextEditor
{
public:
	Backend* backend;
	
	bool running;
	size_t tabWidth;

	Cursor cursor;
	Cursor scroll;
	Cursor term;
	
	std::vector<std::string> lines;
	
	std::string filename;
	bool dirty;



	TextEditor();
	~TextEditor();


	
	void newBuffer();
	bool loadFile(const std::string& filepath);
	bool saveFile();



	void loop();

	void display();
	
	void updateScrollCursor();
	void updateTerminalCursor();
	void displayLinesWithTabs();
	void displayStatusBar();
	
	
	
	void handleInput();
};
