/*
 * My second attempt at making a simple text editor for Linux terminal.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit2.
 *
 * uedit2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit2.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#define BLACK   0
#define RED     1
#define GREEN   2
#define YELLOW  3
#define BLUE    4
#define MAGENTA 5
#define CYAN    6
#define WHITE   7
