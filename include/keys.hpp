/*
 * My second attempt at making a simple text editor for Linux terminal.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit2.
 *
 * uedit2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit2.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#define CTRL_Q		17
#define CTRL_S		19

#define TAB			9
#define ENTER		10
#define ESCAPE		27
#define BACKSPACE	127

#define ARROW_RIGHT	200
#define ARROW_DOWN	201
#define ARROW_LEFT	202
#define ARROW_UP	203

#define DELETE		210
#define HOME		211
#define END			212
#define PAGE_UP		213
#define PAGE_DOWN	214
